from django.urls import path, include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
	path('profil', include('profil.urls')),
	path('story_1', include('story_1.urls'))
]
