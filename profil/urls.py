from django.urls import include, path
from .views import profil

urlpatterns = [
    path('', profil, name='profil'),
]
