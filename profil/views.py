from django.shortcuts import render

kontak = [
	{
		"nama" : "Facebook",
		"gambar" : "img/Facebook-Logo.svg",
		"link" : "facebook.com/solar.flare02"
	},
	{
		"nama" : "Twitter",
		"gambar" : "img/Twitter-Logo.svg",
		"link" : "twitter.com/sheradhien"
	},
	{
		"nama" : "Instagram",
		"gambar" : "img/Instagram-Logo.svg",
		"link" : "instagram.com/sheradhien"
	}
]

pendidikan = [
	{
		"nama" : "SDN 1 Mojolegi",
		"waktu" : "2008 - 2013",
		"deskripsi" : "Masa-masa yang kurang bahagia :[",
		"gambar" : "img/SD.png"
	},
	{
		"nama" : "SMPN 1 Banyudono",
		"waktu" : "2014 - 2016",
		"deskripsi" : "Masa-masa yang lumayan bahagia :>",
		"gambar" : "img/SMP.png"
	},
	{
		"nama" : "SMAN 1 Boyolali",
		"waktu" : "2017 - 2019",
		"deskripsi" : "Masa-masa yang cukup bahagia :/",
		"gambar" : "img/SMA.png"
	}
]

koneksi = [
	{
		"nama" : "Muhammad Alfan",
		"foto" : "img/Alfan.JPG"
	},
	{
		"nama" : "Arya Surya Baskara",
		"foto" : "img/Arya.JPG"
	},
	{
		"nama" : "Dewa Priambada",
		"foto" : "img/Dewa.JPG"
	},
	{
		"nama" : "Jabar",
		"foto" : "img/Jabar.JPG"
	}
]

def profil(request):
	context = {
		"kontak": kontak,
		"pendidikan": pendidikan,
		"koneksi": koneksi
	}
	return render(request, 'main/profil.html', context)
